/*
Copyright 2020 Micronix Solutions, LLC (https://micronix.solutions/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package httpx

import (
	"context"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/micronix-solutions/logging"
)

// #region Types
type ContextKey string
type ViewBag map[string]interface{}

// #endregion

// #region Request Logger
func LogRequestMiddleware(logger logging.Logger) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(wr http.ResponseWriter, req *http.Request) {
			logger.Debug("Begin serve request: " + req.URL.String())
			defer logger.Debug("End serve request: " + req.URL.String())
			next.ServeHTTP(wr, req)
		})
	}
}

// #endregion

// #region ViewBag

func ViewBagMiddleware(next http.Handler) http.Handler {
	attachViewBag := func(wr http.ResponseWriter, req *http.Request) {
		//logger.Debug("enter viewBagMiddleware")
		//defer logger.Debug("exit viewBagMiddleware")
		newContext := context.WithValue(req.Context(), ContextKey("ViewBag"), make(ViewBag))
		newReq := req.WithContext(newContext)
		next.ServeHTTP(wr, newReq)
	}

	return http.HandlerFunc(attachViewBag)
}

func ViewBagFor(req *http.Request) ViewBag {
	return req.Context().Value(ContextKey("ViewBag")).(ViewBag)
}

// #endregion

// #region Sessions
type Session struct {
	Expiration time.Time
	Data       map[string]interface{}
	Messages   []string
	Errors     []string
}

type SessionProvider interface {
	Retrieve(id string) *Session
	New(map[string]interface{}) (string, *Session)
	Delete(id string)
	SetSessionTTL(time.Duration)
	SessionTTL() time.Duration
	SessionMiddleware(http.Handler) http.Handler
}

type InMemorySessionProvider struct {
	Logger     logging.Logger
	sessions   map[uuid.UUID]*Session
	sessionTTL time.Duration
}

func (me *InMemorySessionProvider) Retrieve(id string) *Session {
	uid, err := uuid.Parse(id)
	if err != nil {
		return nil
	}
	return me.sessions[uid]
}

func (me *InMemorySessionProvider) New(data map[string]interface{}) (string, *Session) {
	if me.sessions == nil {
		me.sessions = make(map[uuid.UUID]*Session)
	}
	s := &Session{
		Expiration: time.Now().Add(me.sessionTTL),
		Data:       data,
	}
	if s.Data == nil {
		s.Data = make(map[string]interface{})
	}
	id := uuid.New()
	me.sessions[id] = s
	return id.String(), s
}

func (me *InMemorySessionProvider) Delete(id string) {
	uid, err := uuid.Parse(id)
	if err != nil {
		return
	}
	delete(me.sessions, uid)
}

func (me *InMemorySessionProvider) SetSessionTTL(ttl time.Duration) {
	me.sessionTTL = ttl
}

func (me *InMemorySessionProvider) SessionTTL() time.Duration {
	return me.sessionTTL
}

func (me *InMemorySessionProvider) SessionMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(wr http.ResponseWriter, req *http.Request) {
		me.Logger.Debug("enter SessionMiddleware")
		defer me.Logger.Debug("exit SessionMiddleware")
		sessionID, session := me.extractSessionFromRequest(req)
		if session == nil || session.Expiration.Before(time.Now()) {
			me.Logger.Debug("No / expired session found for cookie value")
			sessionID, session = me.New(nil)
			http.SetCookie(wr, &http.Cookie{
				Name:   "sessid",
				Path:   "/",
				Value:  sessionID,
				MaxAge: int(me.SessionTTL()),
			})
		}
		session.Expiration = time.Now().Add(me.SessionTTL())
		next.ServeHTTP(wr, req.WithContext(context.WithValue(req.Context(), ContextKey("sessionID"), sessionID)))
	})
}

func (me *InMemorySessionProvider) extractSessionFromRequest(req *http.Request) (string, *Session) {
	me.Logger.Debug("enter extractSessionIDFromRequest")
	defer me.Logger.Debug("exit extractSessionIDFromRequest")

	c, err := req.Cookie("sessid")
	if err != nil { //no session cookie set
		me.Logger.Debug(err.Error())
		return "", nil
	}

	return c.Value, me.Retrieve(c.Value)
}

// #endregion
