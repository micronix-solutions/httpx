module gitlab.com/micronix-solutions/httpx

go 1.12

require (
	github.com/google/uuid v1.1.1
	gitlab.com/micronix-solutions/logging v0.1.0
)
